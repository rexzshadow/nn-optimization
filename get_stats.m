testfiledir = 'D:\Class\Thesis\Code\nn-optimization\data\Intital_Result';
matfiles = dir(fullfile(testfiledir, '*.txt'));
nfiles = length(matfiles);
data  = cell(nfiles);
z1 = zeros(1, 1, 'double');
z3 = zeros(1, 1, 'double');
z4 = zeros(1, 1, 'double');
z5 = zeros(1, 1, 'double');

for i = 1 : nfiles
   fid = fopen( fullfile(testfiledir, matfiles(i).name) );
   data{i} = fscanf(fid,'%c');
   C = strsplit(data{i});
   z1(i) = str2double(C(1));
   z2(i) = str2double(C(2));
   z3(i) = str2double(C(3));
   z4(i) = str2double(C(4));
   z5(i) = str2double(C(5));
   fclose(fid);
end

mean1 = mean(z1);
mean2 = mean(z2);
mean3 = mean(z3);
mean4 = mean(z4);
mean5 = mean(z5);

std1 = std(z1);
std2 = std(z2);
std3 = std(z3);
std4 = std(z4);
std5 = std(z5);
file = 'D:\Class\Thesis\Code\nn-optimization\data\workspace.mat';
save(file);
