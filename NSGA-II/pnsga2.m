%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPEA120
% Project Title: Non-dominated Sorting Genetic Algorithm II (NSGA-II)
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

clc;
clear;
close all;

%% Problem Definition
nw = 12;

p = gcp('nocreate'); %get information about current pool
if isempty(p)
    poolobj = parpool(nw); %Create pool
elseif p.NumWorkers == nw

else 

    delete(p);

    poolobj = parpool(nw); %Create pool
end
% CostFunction = @(x) MOP2(x);    % Cost Function
CostFunction = @(x,popnum) LAYERS(x,popnum);

nVar=10;             % Number of Decision Variables
VarSize=[1 nVar];   % Size of Decision Variables Matrix

VarMin= 1;          % Lower Bound of Variables
VarMax= 18;          % Upper Bound of Variables

% Number of Objective Functions
nObj = numel(CostFunction(randi(VarMax, VarSize), 0));
% nObj=numel(CostFunction(unifrnd(VarMin,VarMax,VarSize)));

%% NSGA-II Parameters

%***** User Modifiable Parameters *****
MaxGen=20;         % Maximum Number of Iterations
nPop=20;            % Population Size

pCrossover=0.5;     % Crossover Percentage
pMutation=0.5;      % Mutation Percentage
pStep = 0.1;        % Mutation Step Percentage
mu=0.02;            % Mutation Rate

%***** Calculated Parameters *****
nCrossover=2*round(pCrossover*nPop/2);  % Number of Parnets (Offsprings)
nMutation=round(pMutation*nPop);        % Number of Mutants
sigma=pStep*(VarMax-VarMin);              % Mutation Step Size

%% Initialization

empty_individual.Position=[];
empty_individual.Cost=[];
empty_individual.Rank=[];
empty_individual.DominationSet=[];
empty_individual.DominatedCount=[];
empty_individual.CrowdingDistance=[];

pop=repmat(empty_individual,nPop,1);

%Fill Initial Population with Randomly Generated Candidates
parfor i=1:nPop
%     pop(i).Position=unifrnd(VarMin,VarMax,VarSize);     %Real Valued Candidates
    pop(i).Position = randi(VarMax, VarSize);           %Integer Valued Candidates
    pop(i).Cost=CostFunction(pop(i).Position, i);          %Find Fitness
end

% Non-Dominated Sorting
[pop, F]=NonDominatedSorting(pop);

% Calculate Crowding Distance
pop=CalcCrowdingDistance(pop,F);

% Sort Population
[pop, F]=SortPopulation(pop);

%% NSGA-II Main Loop

for it=1:MaxGen
    
    % Generate Random Indices for Selection, Crossover, and Mutation
    i1 = randi([1 nPop],nCrossover/2);
    i2 = randi([1 nPop],nCrossover/2);
    i  = randi([1 nPop],nMutation);
    
    % Crossover
    disp("Crossover");
    popc=repmat(empty_individual,nCrossover/2,2);
    for k=1:nCrossover/2 
        p1 = pop(i1(k)); %Parent 1
        p2 = pop(i2(k)); %Parent 2
        [popc(k,1).Position, popc(k,2).Position]=Crossover(p1.Position,p2.Position);  
    end
    popc=popc(:);
    
    % Mutation
    disp("Mutation");
    popm=repmat(empty_individual,nMutation,1);
    for k=1:nMutation 
        %p=pop(i(k));
        p = pop(i(k)).Position;
%         popm(k).Position=Mutate(p.Position,mu,sigma);               %Real valued mutation
        popm(k).Position = MutateInt(p, mu, sigma, VarMin, VarMax); %Integer valued mutation
    end
    
    popcm = [popc
             popm];

    % Calculate Fitness for New Children
    disp("Fitness for new children");
    parfor k = 1:(nCrossover+nMutation)
        popcm(k).Cost = CostFunction(popcm(k).Position, k);
    end
    
    % Merge
    pop=[pop
         popcm]; %#ok
     
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);

    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);

    % Sort Population
    pop=SortPopulation(pop);
    
    % Truncate
    pop=pop(1:nPop);
    
    % Non-Dominated Sorting
    [pop, F]=NonDominatedSorting(pop);

    % Calculate Crowding Distance
    pop=CalcCrowdingDistance(pop,F);

    % Sort Population
    [pop, F]=SortPopulation(pop);
    
    % Store F1
    F1=pop(F{1});
    
    % Show Iteration Information
    disp(['Iteration ' num2str(it) ': Number of F1 Members = ' num2str(numel(F1))]);
    
    % Save workspace 
    workspace = "/exports/home/rpan/nn-optimization/workspace"  + num2str(it) + ".mat";
    save(workspace);
    
    % Plot F1 Costs
%     figure(1);
%     PlotCosts(F1);
%     pause(0.01);
    
end
delete(poolobj); %delete pool after code is finished running
%% Results
workspace = '/exports/home/rpan/nn-optimization/workspace.mat';
save(workspace);
disp(['Final Generation: Number of F1 Members = ' num2str(numel(F1))]);
disp('Optimization Terminated.');