import numpy as np
import pickle
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import os
import pickle

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

output = open('x_train.pkl', 'wb')
pickle.dump(x_train, output)
output.close()
output = open('y_train.pkl', 'wb')
pickle.dump(y_train, output)
output.close()
output = open('x_test.pkl', 'wb')
pickle.dump(x_test, output)
output.close()
output = open('y_test.pkl', 'wb')
pickle.dump(y_test, output)
output.close()


pkl_file = open('x_train.pkl', 'rb')
data1 = pickle.load(pkl_file)
if np.array_equal(data1, x_train):
	print('x_train shape:', data1.shape);
pkl_file.close()

pkl_file = open('y_train.pkl', 'rb')
data2 = pickle.load(pkl_file)
if np.array_equal(data2, y_train):
	print('y_train shape:', data2.shape);
pkl_file.close()

pkl_file = open('x_test.pkl', 'rb')
data3 = pickle.load(pkl_file)
if np.array_equal(data3, x_test):
	print('x_test shape:', data3.shape);
pkl_file.close()

pkl_file = open('y_test.pkl', 'rb')
data4 = pickle.load(pkl_file)
if np.array_equal(data4, y_test):
	print('y_test shape:', data4.shape);
pkl_file.close()