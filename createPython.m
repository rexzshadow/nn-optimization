input = [1,16, 28];
fid  = fopen('layers.txt');
tline = fgetl(fid);
tlines =  cell(0);
layers = strings(1,1);
type = strings(1,1);
i = 1;
t = datetime('now');
DateString = datestr(t);
c =  strsplit(DateString);
c = c(1) + "_" + c(2);
c = strrep(c,':','_');
fname = c + ".py";
fname1 = c + ".txt";
fileinsert = "filename = " + '"' + fname1  + '"' +  ";";
while ischar(tline)
    layers(i,1) = tline;
    if(contains(tline, "Conv2D"))
        type(i,1) = "C";
    elseif (contains(tline, "MaxPooling"))
        type(i,1) = "M";
    elseif (contains(tline, "Dense"))
        type(i,1) = "D";
    end  
    tline = fgetl(fid);
    i = i +1;
end
fclose(fid);

reshape = strings(1,1);
fid  = fopen('reshape.txt');
tline = fgetl(fid);
i = 1;
while ischar(tline)
    reshape(i,1) = tline; 
    tline = fgetl(fid);
    i = i +1;
end
mid = "";
fclose(fid);
for n = 1 : length(input)
    
    if(input(n) <= 17)
        
    if (n == 1 && type(input(n))  == "D")
        mid = mid + "model.add(Flatten());";
    end
    
    if( n > 1 && type(input(n))~= "D" && type(input(n-1)) == "D")
        if(contains(layers(input(n-1)), "Dense(512"))
            mid = mid + reshape(1,1);
        elseif(contains(layers(input(n-1)), "Dense(4096"))
            mid = mid + reshape(2,1);
        elseif(contains(layers(input(n-1)), "Dense(1000"))
            mid = mid + reshape(3,1);
        end
    end
    
    if ( n > 1 && type(input(n)) == "D" && type(input(n-1)) ~= "D")
        mid = mid + "model.add(Flatten());";
    end

    mid = mid + layers(input(n));
    
    if (n == length(input) && type(input(n))  ~= "D")
        mid = mid + "model.add(Flatten());";
    end
    end
    
end

mid = mid + fileinsert;
    
top =  fileread('Top.txt');
bot = fileread('Bottom.txt');


folder = "/exports/home/rpan/nn-optimization/Files/";
file = top + mid + bot;
location = folder + fname;

fid=fopen(location, 'wt');
fprintf(fid,'%s' ,file);
fclose(fid);
command = "python " + location;

systemCommand =[command];
system(systemCommand);

resultloc = "/exports/home/rpan/nn-optimization/data/";
resultname = resultloc + fname1;
fid  = fopen(resultname);
tline = fgetl(fid);
results = zeros(1, 1);
i = 1;
while ischar(tline)
    results(i,1) = str2num(tline);
    tline = fgetl(fid);
    i = i +1;
end


