function y=MutateInt(x,mu,sigma, VarMin, VarMax)

    nVar=numel(x);
    
    nMu=ceil(mu*nVar);
    disp(nMu);
    j=randi(nVar, 1);
    
    y=x;
    sigmaInt = floor(sigma);
    
    %deciding + or -
    z = randi(2,1);
    if (z == 1)
        change = x(j)+randi(sigmaInt, 1);
    else
        change = x(j)-randi(sigmaInt, 1);
    end
     
    %Make sure the layer numbers don't go out of bound.
    if (change < VarMin)
        change = change + VarMax;
    end
    
    if (change > VarMax)
        change = change - VarMax;
    end
    
    y(j)= change;

end