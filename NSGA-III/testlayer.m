function z=testlayer(x)

fid  = fopen('layers.txt');
tline = fgetl(fid);
tlines =  cell(0);
layers = strings(1,1);
type = strings(1,1);
i = 1;
t = datetime('now');
DateString = datestr(t);
c =  strsplit(DateString);
c = c(1) + "_" + c(2);
c = strrep(c,':','_');
fname = c + ".py";
fname1 = c + ".txt";
fileinsert = "filename = " + '"' + fname1  + '"' +  ";";
while ischar(tline)
    layers(i,1) = tline;
    if(contains(tline, "Conv2D"))
        type(i,1) = "C";
    elseif (contains(tline, "MaxPooling"))
        type(i,1) = "M";
    elseif (contains(tline, "Dense"))
        type(i,1) = "D";
    end  
    tline = fgetl(fid);
    i = i +1;
end
fclose(fid);

reshape = strings(1,1);
fid  = fopen('reshape.txt');
tline = fgetl(fid);
i = 1;
while ischar(tline)
    reshape(i,1) = tline; 
    tline = fgetl(fid);
    i = i +1;
end
mid = "";
fclose(fid);

pre_type = "z";
prenum = 0;
for n = 1 : length(x)

    if(x(n) <= 17)
        
        if (n == 1 && type(x(n))  == "D")
            mid = mid + "model.add(Flatten()); \r\n";
        end

        if( n > 1 && type(x(n))~= "D" && pre_type == "D")
            if(contains(layers(prenum), "Dense(512"))
                mid = mid + reshape(1,1) + "\r\n";
            elseif(contains(layers(prenum), "Dense(4096"))
                mid = mid + reshape(2,1) + "\r\n";
            elseif(contains(layers(prenum), "Dense(1000"))
                mid = mid + reshape(3,1) + "\r\n";
            end
        end
    
        if ( n > 1 && type(x(n)) == "D" && pre_type ~= "D")
            mid = mid + "model.add(Flatten()); \r\n";
        end

     mid = mid + layers(x(n)) + "\r\n";
    
        if (n == length(x) && type(x(n))  ~= "D")
            mid = mid + "model.add(Flatten()); \r\n";
        end
        pre_type = type(x(n));
        prenum = x(n);
    end
    
end

mid = mid + fileinsert;
    
top =  fileread('Top.txt');
bot = fileread('Bottom.txt');


folder = "";
file = top + mid + bot;
location = "D:\Class\Thesis\Code\nn-optimization\Files\test.py";

fid=fopen(location, 'wt');
fprintf(fid,'%s' ,file);
fclose(fid);
z = 0;