function z=LAYERS(x)

input = x;
%open layer files 
fid  = fopen('/exports/home/rpan/nn-optimization/layers.txt');
tline = fgetl(fid);
tlines =  cell(0);
%set up string array to hold information
layers = strings(1,1);
type = strings(1,1);
i = 1;
%get time stamp for1su file names
t = datetime('now');
DateString = datestr(t);
c =  strsplit(DateString);
%replace space and : with _ to avoid issue running files
c = c(1) + "_" + c(2);
c = strrep(c,':','_');
fname = c + ".py";
fname1 = c + ".txt";
fileinsert = "filename = " + '"' + fname1  + '"' +  ";";
%load layer from file and store layer type
while ischar(tline)
    layers(i,1) = tline;
    if(contains(tline, "Conv2D"))
        type(i,1) = "C";
    elseif (contains(tline, "MaxPooling"))
        type(i,1) = "M";
    elseif (contains(tline, "Dense"))
        type(i,1) = "D";
    end  
    tline = fgetl(fid);
    i = i +1;
end
fclose(fid);

%open and load reshape functions
reshape = strings(1,1);
fid  = fopen('/exports/home/rpan/nn-optimization/reshape.txt');
tline = fgetl(fid);
i = 1;
while ischar(tline)
    reshape(i,1) = tline; 
    tline = fgetl(fid);
    i = i +1;
end

mid = "";
fclose(fid);
%variable to hold previous layer type and number
pre_type = "z";
prenum = 0;
layercount = 1;
for n = 1 : length(x)
    %if the layer number is greather than 17 do nothing for empty layer
    if(x(n) <= 17)
        
        %if first valid layer is dense add Flatten
        if (layercount == 1 && type(x(n))  == "D")
            mid = mid + "model.add(Flatten());";
        end

        %if a none dense layer had a dense layer above it add proper
        %reshape
        if( layercount > 1 && type(x(n))~= "D" && pre_type == "D")
            if(contains(layers(prenum), "Dense(512"))
                mid = mid + reshape(1,1);
            elseif(contains(layers(prenum), "Dense(4096"))
                mid = mid + reshape(2,1);
            elseif(contains(layers(prenum), "Dense(1000"))
                mid = mid + reshape(3,1);
            end
        end
    
        %if current layer is dense but previous layer is not add flatten
        if ( n > 1 && type(x(n)) == "D" && pre_type ~= "D")
            mid = mid + "model.add(Flatten());";
        end

        %adding the layer to string
        mid = mid + layers(x(n));
    
        %set new previous type, number, and increase layer count
        pre_type = type(x(n));
        prenum = x(n);
        layercount = layercount + 1;
    end
    
end

%if the last layer is not dense add flatten
if(pre_type ~= "D")
    mid = mid + "model.add(Flatten());";
end

%add save file name to python file
mid = mid + fileinsert;

%load in top and bottom of the python code
top =  fileread('/exports/home/rpan/nn-optimization/Top.txt');
bot = fileread('/exports/home/rpan/nn-optimization/Bottom.txt');

%combine python cold and save to folder location
folder = "/exports/home/rpan/nn-optimization/Files/";
file = top + mid + bot;
location = folder + fname;
fid=fopen(location, 'wt');
fprintf(fid,'%s' ,file);
fclose(fid);

%kick off python program
command = "python " + location;
systemCommand = [command];
[status, result] = system(systemCommand);
%disp(status);
%disp(result);
%if python program successful
if(status == 0)
    disp("Good layers");
    %load result file and return value
    resultloc = "/exports/home/rpan/nn-optimization/data/";
    resultname = resultloc + fname1;
    fid  = fopen(resultname);
    tline = fgetl(fid);
    results = zeros(1, 1);
    i = 1;
    while ischar(tline)
        results(i,1) = str2num(tline);
        tline = fgetl(fid);
        i = i +1;
    end
    
    %store result and scale it for better math results
    z1 = results(1,1);
    z2 = results(2,1);
    z3 = results(3,1) / 1e7;
    z4 = results(4,1) / 1e3;
    z5 = results(5,1) / 1e4;
    
    %scale std to get better spread
    std3 = 1.933966875636847e+07 / 1e7;
    std3 = std3 / 3;
    std4 = 2.402813290826107e+03/ 1e3;
    std4 = std4 / 4;
    std5 = 2.807963855131005e+04 / 1e4;
    std5 = std5 / 5;
    
    %standarized mean
    mean3 = 2.163027602024292e+07 / 1e7;
    mean4 = 9.616218960679848e+02 / 1e3;
    mean5 = 1.373556668016195e+04 / 1e4;
%     mean4 = 54.15661366;
%     mean5 = 231.12;
    %lims1  = [0.279177360666386 1.9714961112311970];
    lims3  = [std3 mean3];
    lims4  = [std4 mean4];
    lims5  = [std5 mean5];
    
    %Inverse sigmf value to be max
    %z1 = 1 - sigmf(z1, lims1);
    z2 = 1 - z2;
    z3 = sigmf(z3, lims3);
    z4 = sigmf(z4, lims4);
    z5 = sigmf(z5, lims5);
    
    z = [z2 z3 z4 z5];
else
    disp("bad layers");
    %if python program false  return -1

    z = 1 - rand(1,4)/100;
end
end