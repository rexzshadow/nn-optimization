% Project Title: Non-dominated Sorting Genetic Algorithm III (NSGA-III)
% Original Publisher: Yarpiz (www.yarpiz.com)
% 
% Main Algorithm has been modified to allow for parallelized
% implementation.

clc;
clear;
close all;

%% Problem Definition

% CostFunction = @(x) MOP2(x);    % Cost Function
CostFunction = @(x) LAYERS(x);

nVar = 10;                       % Number of Decision Variables
VarSize = [1 nVar];             % Size of Decision Variables Matrix

VarMin = 1;                    % Lower Bound of Variables
VarMax = 18;                     % Upper Bound of Variables

% Number of Objective Functions
% nObj = numel(CostFunction(unifrnd(VarMin, VarMax, VarSize)));
nObj = numel(CostFunction(randi(VarMax, VarSize)));

%% NSGA-II Parameters

%***** User Modifiable Parameters *****
MaxGen = 4;        % Maximum Number of Generations
nPop = 10;          % Population Size

pCrossover = 0.5;   % Crossover Percentage
pMutation = 0.5;    % Mutation Percentage
pStep = 0.1;        % Mutation Step Percentage
mu = 0.02;          % Mutation Rate

nDivision = 10;     % Number of Reference Point Divisions

%***** Calculated Parameters *****
nCrossover = 2*round(pCrossover*nPop/2);    % Number of Parents+Offspring
nMutation = round(pMutation*nPop);          % Number of Mutants
sigma = pStep*(VarMax-VarMin);              % Mutation Step Size

% Generating Reference Points
Zr = GenerateReferencePoints(nObj, nDivision);

%% Collect Parameters into Structure 
params.nPop = nPop;
params.Zr = Zr;
params.nZr = size(Zr,2);
params.zmin = [];
params.zmax = [];
params.smin = [];

%% Initialization

disp('Staring NSGA-III ...');

empty_individual.Position = [];
empty_individual.Cost = [];
empty_individual.Rank = [];
empty_individual.DominationSet = [];
empty_individual.DominatedCount = [];
empty_individual.NormalizedCost = [];
empty_individual.AssociatedRef = [];
empty_individual.DistanceToAssociatedRef = [];

%% Initial Population
pop = repmat(empty_individual, nPop, 1);                %Create Empty Population

%Fill Initial Population with Randomly Generated Candidates
parfor i = 1:nPop
%     pop(i).Position = unifrnd(VarMin, VarMax, VarSize); %Real Valued Candidates
    pop(i).Position = randi(VarMax, VarSize);           %Integer Valued Candidates
    pop(i).Cost = CostFunction(pop(i).Position);        %Find Fitness
end

% Sort Population and Perform Selection
[pop, F, params] = SortAndSelectPopulation(pop, params);

%% NSGA-II Main Loop

for it = 1:MaxGen
 
    % Generate Random Indices for Selection, Crossover, and Mutation
    i1 = randi([1 nPop],nCrossover/2);
    i2 = randi([1 nPop],nCrossover/2);
    i  = randi([1 nPop],nMutation);
    
    % Crossover
    popc = repmat(empty_individual, nCrossover/2, 2);
    for k = 1:nCrossover/2
        p1 = pop(i1(k)); %Parent 1
        p2 = pop(i2(k)); %Parent 2

        [popc(k, 1).Position, popc(k, 2).Position] = Crossover(p1.Position, p2.Position);
    end
    popc = popc(:);

    % Mutation
    popm = repmat(empty_individual, nMutation, 1);
    for k = 1:nMutation
        p = pop(i(k)).Position;
        
        %popm(k).Position = Mutate(p, mu, sigma);
        popm(k).Position = MutateInt(p, mu, sigma, VarMin, VarMax);
    end
    
    % Merge New Children
    popcm = [popc
             popm];
         
    % Calculate Fitness for New Children
    parfor k = 1:(nCrossover+nMutation)
        popcm(k).Cost = CostFunction(popcm(k).Position);
    end

    % Merge
    pop = [pop
           popcm]; 
    
    % Sort Population and Perform Selection
    [pop, F, params] = SortAndSelectPopulation(pop, params);
    
    % Store F1
    F1 = pop(F{1});

    % Show Iteration Information
    disp(['Generation ' num2str(it) ': Number of F1 Members = ' num2str(numel(F1))]);
    workspace = "/exports/home/rpan/nn-optimization/workspace"  + num2str(it) + ".mat";
    save(workspace);
    % Plot F1 Costs
    %figure(1);
    %PlotCosts(F1);
    %pause(0.01);
 
end

%% Results
workspace = '/exports/home/rpan/nn-optimization/workspace.mat';
save(workspace);
disp(['Final Generation: Number of F1 Members = ' num2str(numel(F1))]);
disp('Optimization Terminated.');


