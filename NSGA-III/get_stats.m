%location of all the results
testfiledir = 'D:\Class\Thesis\Intital_Result';
matfiles = dir(fullfile(testfiledir, '*.txt'));
nfiles = length(matfiles);
data  = cell(nfiles);

%initialize array to hold fitness value
z1 = zeros(1, 1, 'double');
z2 = zeros(1, 1, 'double');
z3 = zeros(1, 1, 'double');
z4 = zeros(1, 1, 'double');
z5 = zeros(1, 1, 'double');

%load all values from all files
for i = 1 : nfiles
   fid = fopen( fullfile(testfiledir, matfiles(i).name) );
   data{i} = fscanf(fid,'%c');
   C = strsplit(data{i});
   z1(i) = str2double(C(1));
   z2(i) = str2double(C(2));
   z3(i) = str2double(C(3));
   z4(i) = str2double(C(4));
   z5(i) = str2double(C(5));
   fclose(fid);
end

%calcluate mean
mean1 = mean(z1);
mean2 = mean(z2);
mean3 = mean(z3);
mean4 = mean(z4);
mean5 = mean(z5);

%calculate std
std1 = std(z1);
std2 = std(z2);
std3 = std(z3);
std4 = std(z4);
std5 = std(z5);

%plot graph
% yyaxis left
% z3 = z3/1e7
% histogram(z3);
% lims(1) = std3 / 1e7;
% lims(1) = lims(1) / 3;
% lims(2) = mean3 / 1e7;
% x = -1:.1:10;
% sigz = 1-sigmf(x, lims);
% yyaxis right
% plot(x, sigz);

% mean4 = 54.15661366;
yyaxis left
z4 = z4/1e3
histogram(z4);
lims(1) = std4 / 1e3;
lims(1) = lims(1)/4;
lims(2) = mean4 / 1e3;
x = -1:.1:10;
sigz = 1- sigmf(x, lims);
yyaxis right
plot(x, sigz);

% mean5 = 231.12;
% yyaxis left
% z5 = z5/1e4;
% histogram(z5);
% lims(1) = std5 / 1e4;
% lims(1) = lims(1)/5;
% lims(2) = mean5 / 1e4;
% x = -1:.1:10;
% sigz = 1- sigmf(x, lims);
% yyaxis right
% plot(x, sigz);
