function [ y ] = sigmf( x, lims )

a = lims(1);
c = lims(2);

y = 1 ./ (1 + exp(-a*(x-c)));
end
